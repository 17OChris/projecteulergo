echo off
set arg1=%1

set dest=problems\%arg1%\problem.go

mkdir problems\%arg1%

echo package %arg1% >> %dest%
echo[ >> %dest%
echo import "fmt" >> %dest%
echo[ >> %dest%
echo func Run() { >> %dest%
echo    answer := 0 >> %dest%
echo    fmt.Printf("Answer = %%d\n", answer) >> %dest%
echo } >> %dest%