package nineteen

import "fmt"

var days = []int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
var daysLeap = []int{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

func Run() {
	answer := 0
	dayTally := 1
	for year := 1900; year < 2001; year++ {
		leapYear := isLeapYear(year)
		days := getDayPlan(leapYear)

		for _, val := range days {
			if dayTally%7 == 0 && year > 1900 {
				answer++
			}
			dayTally += val
		}

	}
	fmt.Printf("Answer = %d\n", answer)
}

func isLeapYear(year int) bool {
	leapYear := false
	if (year%400 == 0) || (year%100 != 0 && year%4 == 0) {
		leapYear = true
	}
	return leapYear
}

func getDayPlan(leapYear bool) []int {
	if leapYear {
		return daysLeap
	}
	return days
}
