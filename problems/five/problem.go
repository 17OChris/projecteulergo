package five

import "fmt"

// if it divides by these, it will be divisible by all numbers below
// we don't need 19 or 20 as we are incrementing by the product of 19 & 20
var numbers = []int{11, 12, 13, 14, 15, 16, 17, 18}

func Run() {
	answer := 0

	// Calculate the increment number by working out the first common number between 20 and 19
	increment := 20 * 19 // 20*19
	i := 0
	// while the answer is 0 ...
	for answer == 0 {
		// increase i by the increment
		i += increment
		equallyDivisible := true
		// loop through the numbers and check for any that can't be equally divided.
		for _, number := range numbers {
			if i%number != 0 {
				equallyDivisible = false
				break
			}
		}

		if equallyDivisible {
			answer = i
		}

	}

	fmt.Printf("Answer = %d\n", answer)
}
