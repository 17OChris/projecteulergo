package twentytwo

import (
	"fmt"
	"sort"
	"strings"

	"gitlab.com/17OChris/projecteulergo/utils"
)

var alphabet []string

func Run() {
	answer := 0
	// create an alphabet array
	alphabet = strings.Split("ABCDEFGHIJKLMNOPQRSTUVWXYZ", "")
	//Create an array to store the names.
	names := make([]string, 0)
	//Read the File to string array
	lines, _ := utils.ReadLines("./problems/twentytwo/names.txt")
	// loop through each line and then each name -> push to names store.
	for _, line := range lines {
		lineNames := strings.Split(line, ",")
		for _, n := range lineNames {
			names = append(names, strings.ReplaceAll(n, "\"", ""))
		}
	}
	//Sort alphabetically.
	sort.Strings(names)
	// Loop through each name and add the values
	for index, name := range names {
		nameTotal := 0
		nameSplit := strings.Split(name, "")
		for _, letter := range nameSplit {
			pos := getAlphabetPos(letter)
			nameTotal += pos
		}
		answer += (nameTotal * (index + 1))
	}
	fmt.Printf("Answer = %d\n", answer)
}

// utility method to get the position of the letter from the alphabet
func getAlphabetPos(letter string) int {
	for i, val := range alphabet {
		if val == letter {
			return i + 1
		}
	}
	return 0
}
