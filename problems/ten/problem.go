package ten

import (
	"fmt"
	"math"
)

const max = 2000000

func Run() {
	// starts at 2 as 2 is the first prime
	answer := 2

	for i := 3; i < max; i += 2 {
		if isPrime(i) {
			answer += i
		}
	}

	fmt.Printf("Answer = %d\n", answer)
}

// using https://www.planetmath.org/howtofindwhetheragivennumberisprimeornot
func isPrime(n int) bool {
	if n%2 == 0 {
		return false
	}

	k := int(math.Floor(math.Sqrt(float64(n))))
	for i := 3; i <= k; i += 2 {
		if n%i == 0 {
			return false
		}
	}
	return true
}
