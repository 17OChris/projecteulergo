package fifteen

import "fmt"

// note although there are 2 PointYs and 2 PointXs, theres 3 POINTS
//    Y   Y  Y
//    __  __
// X |__||__||
// X |__||__||

/*
Explanation
I worked out / remembered from a while ago that theres a nice algorithm
Which can solve this without recursively checking the routes.

Say we have a 2x2 grid:

x--x--x
|  |  |
x--x--x
|  |  |
x--x--x

Notice that even though there are 2 columns, theres 3 points? That's important to factor in.

NOTE * IF either x.PontX OR x.PointY == 1 (ie if it belongs to the first column or row) the value is 1 by default.
The formula to work out the maximum routes to get to a position is:
x = {PointX: 2, PointY 2, Val = ?}
y = point above ({PointX: 1, PointY 1, Val = 1})
z = point to the left ({PointX: 1, PointY 2, Val = 1})

x.Val = y.Val + z.Val

If we use the grid above:

1--1--1
|  |  |
1--2--3
|  |  |
1--3--6

Here is what the grid looks like with the formula

1-------1----------1
|       |          |
1----(1+1=2)----(2+1=3)
|       |          |
1----(1+2=3)----(3+3=6)


*/

const gridWidth = 20
const gridHeight = 20

// Slot points will always start at 1
// so for a 2x2 grid the max points would be PointX=3 && PointY=3
type Slot struct {
	PointX int
	PointY int
	Val    int
}

func Run() {
	answer := 0
	slots := generateSlots()
	answer = slots[len(slots)-1].Val
	fmt.Printf("Answer = %d\n", answer)
}

func generateSlots() []Slot {
	s := make([]Slot, 0)

	for y := 0; y <= gridHeight; y++ {
		for x := 0; x <= gridWidth; x++ {
			// The plus + 1 is to ensure we don't start at 0
			slot := Slot{
				PointX: x + 1,
				PointY: y + 1,
				Val:    1,
			}
			if slot.PointX > 1 || slot.PointY > 1 {
				toTheLeft := getSlot(slot.PointY, slot.PointX-1, s)
				above := getSlot(slot.PointY-1, slot.PointX, s)
				slot.Val = toTheLeft.Val + above.Val
			}
			s = append(s, slot)
		}
	}
	return s
}

// Helper method to quickly get a slot by its X and Y Pos.
func getSlot(pointY int, pointX int, slots []Slot) (s Slot) {
	for _, val := range slots {
		if val.PointY == pointY && val.PointX == pointX {
			s = val
			break
		}
	}
	return
}
