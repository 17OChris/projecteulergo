package thirteen

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

type X struct {
	sync.Mutex
	Total string
}

var x X = X{
	Total: "",
}

func Run() {
	// declare and initialise the answer
	answer := ""

	// Read the lines from the text file.
	lines, err := readLines("./problems/thirteen/numbers.txt")

	if err != nil {
		panic(err)
	}
	// loop through each line and add it to the answer
	for _, line := range lines {
		answer = addTwoStrings(answer, line)
	}

	// we only care about the first 10 characters
	answer = answer[:10]
	fmt.Printf("Answer = %s\n", answer)
}

func addTwoStrings(strOne string, strTwo string) string {
	// declare and initialise the total
	total := ""
	// Make sure both strings are the same size.

	if len(strOne) < len(strTwo) {
		for len(strOne) < len(strTwo) {
			strOne = "0" + strOne
		}
	} else if len(strTwo) < len(strOne) {
		for len(strTwo) < len(strOne) {
			strTwo = "0" + strTwo
		}
	}

	carryOver := 0

	// start from the back and convert each string to an int
	// then add together and work out the carry adjustment
	for i := len(strOne) - 1; i >= 0; i-- {
		a, _ := strconv.Atoi(string(strOne[i]))
		b, _ := strconv.Atoi(string(strTwo[i]))
		c := a + b + carryOver
		carryOver = 0

		for c >= 10 {
			c -= 10
			carryOver++
		}
		cS := strconv.Itoa(c)
		total = cS + total
	}

	// theres a chance that the last number has a carry,
	// So here we do one last check
	if carryOver > 0 {
		cS := strconv.Itoa(carryOver)
		total = cS + total
		fmt.Println(carryOver)
	}

	return total

}

// https://stackoverflow.com/a/18479916 - code snippet used from here.
// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, strings.TrimSpace(scanner.Text()))
	}
	return lines, scanner.Err()
}
