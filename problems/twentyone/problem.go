package twentyone

import "fmt"

const max = 10000

func Run() {
	answer := 0
	amicableNumbers := make([]int, 0)

	for i := 2; i < max; i++ {
		a := sumOfDividers(i)
		b := sumOfDividers(a)

		if b != i || a == b {
			continue
		}

		if !existsInArray(a, amicableNumbers) && a < max {
			amicableNumbers = append(amicableNumbers, a)
			answer += a
		}

		if !existsInArray(b, amicableNumbers) && b < max {
			amicableNumbers = append(amicableNumbers, b)
			answer += b
		}
	}

	fmt.Printf("Answer = %d\n", answer)
}

func sumOfDividers(num int) int {
	if num == 1 {
		return 0
	}
	total := 1
	halfNum := 0
	if num%2 == 0 {
		halfNum = num / 2
	} else {
		halfNum = (num + 1) / 2
	}
	for i := 2; i <= halfNum; i++ {
		if num%i == 0 {
			total += i
		}
	}
	return total
}

func existsInArray(num int, array []int) bool {
	for _, val := range array {
		if val == num {
			return true
		}
	}
	return false
}
