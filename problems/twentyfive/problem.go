package twentyfive

import (
	"fmt"

	"gitlab.com/17OChris/projecteulergo/utils"
)

var termsArray = []string{"1", "1"}

const max = 1000

func Run() {
	term := 2
	for len(termsArray[1]) != max {
		termsArray = f(termsArray[1], termsArray[0])
		term++
	}
	fmt.Printf("Answer = %d\n", term)
}

func f(nMinusOne string, nMinusTwo string) []string {
	n := utils.AddTwoStrings(nMinusOne, nMinusTwo)
	return []string{nMinusOne, n}
}
