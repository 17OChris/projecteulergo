package twentysix

import (
	"fmt"
	"strconv"
)

var history []string

func Run() {
	answer := 0
	largest := ""
	history = make([]string, 2)
	history[0] = ""
	history[1] = ""

	for i := 2; i < 1000; i++ {
		history = append(history, "")
		manualDivision(1, i)
	}

	for index, val := range history {
		if index < 2 {
			continue
		}
		if len(val) > len(largest) {
			largest = val
			answer = index
		}
	}
	fmt.Printf("Answer = %d (%s)\n", answer, largest)
}

func manualDivision(numerator int, denominator int) {
	valueOfDivision := numerator / denominator
	mod := numerator % denominator

	if mod == 0 {
		// not valid as its not recurring.
		history[len(history)-1] = ""
		return
	}

	str := strconv.Itoa(valueOfDivision)
	history[len(history)-1] = fmt.Sprintf("%s%s", history[len(history)-1], str)

	if len(history[len(history)-1])%2 == 0 {
		// check for pattern
		pattern := getLongestPattern(history[len(history)-1])
		if pattern != "" {
			// We have a pattern
			history[len(history)-1] = pattern
			return
		}
	}

	if mod*10 != numerator {
		manualDivision(mod*10, denominator)
	} else {
		x := history[len(history)-1]
		history[len(history)-1] = x[len(x)-1:]
	}

}

// For this method, we want to loop through each character, and extend by 1 to see if we find a pattern.
// start a 0, increment, then start at 2, then incrent etc
func getLongestPattern(potentialPattern string) string {

	for startingIndex := 0; startingIndex < len(potentialPattern)-2; startingIndex++ {
		// Move the testPatternStarting index across one each time
		for length := 2; (length+startingIndex) <= len(potentialPattern) && ((length+startingIndex)*2) <= len(potentialPattern); length++ {
			aStart := startingIndex
			aEnd := startingIndex + length
			bStart := aEnd
			bEnd := startingIndex + (length * 2)
			currentPattern := potentialPattern[aStart:aEnd]
			testPattern := potentialPattern[bStart:bEnd]

			if currentPattern == testPattern {
				// need to check if the next number along matches the first character of aStart
				/*
					IF the testPattern is the last part of the string, return "" so that we loop through again and wait for another iteration atleast
					ELSE check if we can do a third check now - if we cant then return ""
					ELSE next Pattern = cStart & cEnd and check if they all match.
				*/

				if bEnd == len(potentialPattern)-2 {
					return ""
				}
				// check if theres enough to do a third check.
				if len(potentialPattern)-length < bEnd {
					return ""
				}
				cStart := bEnd
				cEnd := startingIndex + (length * 3)
				finalPattern := potentialPattern[cStart:cEnd]
				if finalPattern == testPattern {
					return currentPattern
				}
			}
		}
	}

	return ""
}
