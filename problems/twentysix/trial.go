package twentysix

import "fmt"

// Methods that didnt work

// For this method, we want to loop through each character, and extend by 1 to see if we find a pattern.
// start a 0, increment, then start at 2, then incrent etc
func getLongestPatternThirdsAttempt(potentialPattern string, output bool) string {

	for startingIndex := len(potentialPattern); startingIndex > 0; startingIndex-- {
		// Move the testPatternStarting index across one each time
		isEven := startingIndex%2 == 0
		maxLength := 1
		if isEven {
			maxLength = startingIndex / 2
		} else {
			maxLength = ((startingIndex - 1) / 2) + 1
		}

		for length := maxLength; maxLength > 0; length-- {
			// [1,2,3,4,5,6,7,8,9]

			// [1,2,3,4,5,6,7,8,9]
			//  bs    beae      as
			aStart := startingIndex - length
			aEnd := startingIndex
			bStart := aStart - length
			bEnd := aStart
			currentPattern := potentialPattern[aStart:aEnd]
			testPattern := potentialPattern[bStart:bEnd]

			if output {
				fmt.Printf("currentPattern = %s, testPattern = %s\n\n", currentPattern, testPattern)
			}

			if currentPattern == testPattern {
				return currentPattern
			}
		}
	}

	return ""
}

// For this method, we want to loop through each character, and extend by 1 to see if we find a pattern.
// start a 0, increment, then start at 2, then incrent etc
func getLongestPatternSecondAttempt(potentialPattern string, output bool) string {

	for startingIndex := 0; startingIndex < len(potentialPattern)-2; startingIndex++ {
		// Move the testPatternStarting index across one each time
		for length := 2; (length+startingIndex) <= len(potentialPattern) && ((length+startingIndex)*2) <= len(potentialPattern); length++ {
			aStart := startingIndex
			aEnd := startingIndex + length
			bStart := aEnd
			bEnd := startingIndex + (length * 2)
			currentPattern := potentialPattern[aStart:aEnd]
			testPattern := potentialPattern[bStart:bEnd]

			if output {
				fmt.Printf("currentPattern = %s, testPattern = %s\n\n", currentPattern, testPattern)
			}

			if currentPattern == testPattern {

				// it's a pattern!
				// @todo work out if its a single number like 0.3333333333333333333333333333333333333333 ...
				return currentPattern
			}
		}
	}

	return ""
}

/**
start at latest
this will always be even
*/
func getLongestPatternFirstAttempt(potentialPattern string) string {
	// fmt.Printf("Potential pattern = %s\n", potentialPattern)
	// 10 /2 = 5 which = 4th index (0,1,2,3,4)
	// 0 1 2 3 4 5 6 7 8 9
	// 0 1 2 3 4 5 6 7 8 9 10 11
	halfWayPoint := (len(potentialPattern) / 2)

	// check if part 1 == part 2
	// if not, halfwayPoint--

	for halfWayPoint >= 1 {

		strOne := potentialPattern[0:halfWayPoint]
		strTwo := potentialPattern[halfWayPoint:(halfWayPoint * 2)]

		if strOne != strTwo {
			halfWayPoint--
			continue
		}
		// match

		// now we need to know if half of these match too,
		//  if they do then we have caught the pattern at the second, third or fourth etc iteration.

		for {
			// minimum equal divisor
			divisor := -1
			lengthOfStrOne := len(strOne)
			for i := 2; i < len(strOne); i++ {
				if lengthOfStrOne%i == 0 {
					divisor = i
					break
				}
			}

			// that is probably the pattern!
			if divisor == -1 {
				return strOne
			}

			if divisor%2 != 0 {
				strOne = strOne[0 : (divisor-1)/divisor]
				continue
			}

			// the length of strOne is even, now we want to split those and see if they match.
			// if they do match, then we want to split the first one again and see if that matches

			alpha := strOne[0 : (lengthOfStrOne/divisor)-1]
			beta := strOne[lengthOfStrOne/divisor:]

			if alpha != beta {
				return strOne
			}

			// it matches!

			strOne = alpha

		}
	}

	return ""
}
