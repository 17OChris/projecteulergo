package fourteen

import (
	"fmt"
	"sync"
)

// Create a struct with a mutex for locking writes in go routines.
type Num struct {
	sync.RWMutex
	Answer      int
	ChainLength int
	Stop        bool
}

var num Num = Num{
	Answer:      1,
	ChainLength: 1,
	Stop:        false,
}

const aim = 1000000

var wg sync.WaitGroup

func Run() {

	increment := 1
	for !num.Stop {
		for i := 0; i < 100; i++ {
			wg.Add(1)
			go f(increment)
			increment++
		}
		wg.Wait()

		if increment >= aim {
			num.Lock()
			num.Stop = true
			num.Unlock()
		}

	}
	fmt.Printf("Answer = %d\n", num.Answer)
}

func f(number int) {
	defer wg.Done()

	total := calculateChainLength(number)
	if total <= num.ChainLength {
		return
	}

	num.Lock()
	defer num.Unlock()

	num.ChainLength = total
	num.Answer = number
}

func calculateChainLength(number int) int {
	// Starting at 2 for the number itself and 1
	total := 2
	numberTwo := number
	for {
		numberTwo = doAlgo(numberTwo)
		if numberTwo > 1 {
			total++
			continue
		}
		break
	}
	return total
}

func doAlgo(number int) int {
	if number%2 == 0 {
		return number / 2
	}
	return (number * 3) + 1
}
