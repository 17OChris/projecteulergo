package twentyfour

import (
	"fmt"
	"sort"
)

var max = 1000000
var stack []int = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

//https://www.geeksforgeeks.org/lexicographic-permutations-of-string/
func Run() {

	iteration := 1
	currentPerm := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Printf("Perm = %v and i = %d\n", currentPerm, iteration)
	for iteration < max {
		currentPerm = getNextSmallestPossibility(currentPerm)
		iteration++
		if len(currentPerm) < 1 {
			break
		}
	}
	fmt.Printf("Answer = %v\n", currentPerm)
}

func getNextSmallestPossibility(current []int) []int {
	// step 1 - find the right most (further to the right) character where the next character is smallest than it

	n := make([]int, 0)
	for i := len(current) - 2; i >= 0; i-- {
		numberToLookFor := current[i] + 1

		numberFound := false
		for !numberFound && numberToLookFor <= stack[len(stack)-1] {
			for k := i + 1; k < len(current); k++ {

				if current[k] != numberToLookFor {
					continue
				}
				numberFound = true
				a := current[i]
				b := current[k]

				// swap the values around
				current[i] = b
				current[k] = a

				// Step 2 - sort the rest of the numbers in lowest to highest starting at pos i - don't touch the ones before

				firstPart := current[0:(i + 1)]

				endPart := current[(i + 1):]

				sort.Ints(endPart)

				n = append(n, firstPart...)
				n = append(n, endPart...)
				break
			}
			if !numberFound {
				numberToLookFor++
			}
		}

		if numberFound {
			break
		}
	}
	return n
}
