package sixteen

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/17OChris/projecteulergo/utils"
)

func Run() {
	answer := calculateHugePowerOf(2, 1000)
	answer = calcSumOfEachDigitInNumber(answer)
	fmt.Printf("Answer = %s\n", answer)
}

func calcSumOfEachDigitInNumber(strNum string) string {
	split := strings.Split(strNum, "")
	total := "0"
	for _, str := range split {
		total = utils.AddTwoStrings(total, str)
	}
	return total
}

func calculateHugePowerOf(n int, toThePowerOf int) string {
	total := ""

	stringN := strconv.Itoa(n)

	for i := 1; i < toThePowerOf; i++ {
		for k := 1; k <= n; k++ {
			// total = addTwoStrings(total, stringN)
			total = utils.AddTwoStrings(total, stringN)
		}
		stringN = total
		if i < toThePowerOf-1 {
			total = "0"
		}
	}
	return total
}
