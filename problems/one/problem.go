package one

import "fmt"

const max = 1000

func One() {
	sum := 0
	for i := 3; i < max; i++ {
		if i%3 == 0 || i%5 == 0 {
			sum += i
		}
	}

	fmt.Printf("Sum = %d", sum)
}
