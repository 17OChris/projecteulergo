package twenty

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/17OChris/projecteulergo/utils"
)

func Run() {
	total := "100"
	answer := 0
	for i := 99; i > 0; i-- {
		total = factorial(total, i)
	}
	arr := strings.Split(total, "")
	for _, val := range arr {
		i, _ := strconv.Atoi(val)
		answer += i
	}
	// split the string and add the individual numbers
	fmt.Printf("Answer = %d\n", answer)
}

func factorial(strOne string, number int) string {
	total := strOne
	for i := 0; i < number; i++ {
		total = utils.AddTwoStrings(total, strOne)
	}
	return total
}
