package twelve

const aim = 500

// @todo needs commenting
func getFactors(n int) []int {
	x := n
	factors := make([]int, 0)
	for i := 2; i <= x; i++ {
		if x%i == 0 {
			x /= i
			factors = append(factors, i)
			i--
			continue
		}
		if i == x {
			factors = append(factors, i)
		}
	}
	return factors
}

func totalDivisors(numbers []int) int {
	appearances := make([]int, 0)

	for _, val := range getUniqueEntries(numbers) {
		count := countAppearancesInArray(val, numbers)
		appearances = append(appearances, count)
	}

	total := 1

	for _, val := range appearances {
		total *= (val + 1)
	}
	return total

}

func inArray(n int, arr []int) bool {
	for _, val := range arr {
		if val == n {
			return true
		}
	}
	return false
}

func countAppearancesInArray(n int, arr []int) int {
	count := 0
	for _, val := range arr {
		if val == n {
			count++
		}
	}
	return count
}

func getUniqueEntries(arr []int) []int {
	unique := make([]int, 0)
	for _, val := range arr {
		if !inArray(val, unique) {
			unique = append(unique, val)
		}
	}
	return unique
}
