package twelve

import (
	"fmt"
	"sync"
)

type X struct {
	sync.RWMutex
	Answer   int
	DontStop bool
}

var x X = X{
	Answer:   0,
	DontStop: true,
}

var wg sync.WaitGroup

// 2021/04/09 14:51:21 Problem took 29.6557ms
func Run() {
	number := 0
	increment := 1
	for x.DontStop {
		for i := 0; i < 100; i++ {
			number += increment
			increment++
			wg.Add(1)
			go f(number)
		}
		wg.Wait()

		if x.Answer > aim {
			x.Lock()
			x.DontStop = false
			x.Unlock()
		}
	}
	fmt.Printf("Answer = %d\n", x.Answer)
}

// followed logic from here - https://www.wikihow.com/Determine-the-Number-of-Divisors-of-an-Integer
func f(num int) {
	defer wg.Done()
	factors := getFactors(num)
	divisors := totalDivisors(factors)
	x.Lock()
	if divisors > aim {
		if x.Answer == 0 || num < x.Answer {
			x.Answer = num
		}
	}
	x.Unlock()
}
