package twelve

import (
	"fmt"
)

// @todo needs commenting
// 2021/04/09 14:48:49 Problem took 69.1483ms
func RunSync() {

	// Declare and initialise the answer and divisors
	answer := 0
	divisors := 0

	number := 0
	increment := 1
	for divisors <= aim {
		// calculate the new value of number
		number += increment
		//increment by one
		increment++
		factors := getFactors(number)
		divisors = totalDivisors(factors)
	}
	answer = number
	fmt.Printf("Answer = %d\n", answer)
}
