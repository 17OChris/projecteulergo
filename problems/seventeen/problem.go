package seventeen

import (
	"fmt"
	"math"
)

var units []string = []string{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}
var unitsSpecial []string = []string{"zero", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"}
var tens []string = []string{"zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"}
var hundred string = "hundred"
var thousand string = "thousand"

var answer int = 0

const max = 1000

func Run() {

	for i := 1; i <= max; i++ {
		answer += calculateLettersOfNumber(i)
	}
	fmt.Printf("Answer = %d\n", answer)
}

func calculateLettersOfNumber(number int) (length int) {

	length = 0

	if number%1000 == 0 {
		length = len(units[number/1000]) + len(thousand)
		return
	}

	if number%100 == 0 {
		length = len(units[number/100]) + len(hundred)
		return
	}

	if number > 100 {
		decimal := float64(number / 100)
		v := int(math.Floor(decimal))
		length = len(units[v]) + len(hundred) + 3 // x hundered and ...
		number -= (v * 100)
	}

	if number%10 == 0 {
		length += len(tens[number/10])
	} else if number < 10 {
		length += len(units[number])
	} else if number < 20 {
		length += len(unitsSpecial[number-10])
	} else {
		decimal := float64(number / 10)
		v := int(math.Floor(decimal))
		length += len(tens[v])
		length += len(units[number-(v*10)])
	}

	return
}
