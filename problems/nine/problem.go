package nine

import (
	"fmt"
)

// the aim we are trying to make adding up
const aim = 1000

// Simple struct to handle a,b and c values
type obj struct {
	A int
	B int
	C int
}

func Run() {
	answer := 0

	possibleNumbers := make([]obj, 0)

	// get all possibilities which add up to the AIM.
	for a := 1; a < aim; a++ {
		for b := a + 1; b < aim; b++ {
			for c := b + 1; c < aim; c++ {
				if a+b+c == aim {
					abc := obj{A: a, B: b, C: c}
					possibleNumbers = append(possibleNumbers, abc)
				}
			}
		}
	}

	for _, val := range possibleNumbers {
		isTriplet, product := pythagoreanTriplet(val)
		if isTriplet {
			answer = product
			break
		}
	}

	fmt.Printf("Answer = %d\n", answer)
}

func pythagoreanTriplet(val obj) (bool, int) {
	if (val.A*val.A)+(val.B*val.B) == (val.C * val.C) {
		return true, val.A * val.B * val.C
	}
	return false, -1
}
