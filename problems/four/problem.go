package four

import (
	"fmt"
	"math"
	"sort"
	"strconv"
)

// Declare the range of numbers to use.
const min = 100
const max = 999

func Run() {
	// declare + initialise numbers
	answer := 0

	// Create a slice of int to store our calculated numbers
	numbers := make([]int, 0)

	// Calculate all the possible numbers
	for i := min; i <= max; i++ {
		for k := i; k <= max; k++ {
			total := i * k
			numbers = append(numbers, total)
		}
	}

	// Sort the array from smallest to largest - this way we can interate backwards to find
	// the first palindrome and know it's the largest!
	sort.Ints(numbers)

	for i := len(numbers) - 1; i >= 0; i-- {
		n := numbers[i]
		if isPalindrome(n) {
			answer = n
			break
		}
	}

	fmt.Printf("Answer = %d\n", answer)
}

func isPalindrome(n int) bool {
	// convert to a string
	s2 := strconv.Itoa(n)
	//Decalare the two splits to compare later
	splitOne := ""
	splitTwo := ""

	// Save the length for calculations ahead.
	length := len(s2)

	if len(s2)%2 == 0 {
		splitOne = s2[:length/2]
		splitTwo = s2[length/2:]
	} else {
		halfPoint := int(math.Floor(float64(length / 2)))
		splitOne = s2[:halfPoint]
		splitTwo = s2[halfPoint+1:]
	}

	//Reverse the second split
	splitTwo = reverse(splitTwo)

	return splitOne == splitTwo
}

func reverse(s string) string {
	rns := []rune(s) // convert to rune
	for i, j := 0, len(rns)-1; i < j; i, j = i+1, j-1 {

		// swap the letters of the string,
		// like first with last and so on.
		rns[i], rns[j] = rns[j], rns[i]
	}

	// return the reversed string.
	return string(rns)
}
