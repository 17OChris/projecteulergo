package eleven

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type gridPos struct {
	Val int
	X   int
	Y   int
}

type coOrd struct {
	X int
	Y int
}

const adjNumber = 4

var numbers []gridPos
var maxX int = 0
var maxY int = 0

func Run() {
	answer := 0

	// our storage array
	numbers = make([]gridPos, 0)

	// Read the lines from the numbers text
	lines, err := readLines("./problems/eleven/numbers.txt")

	if err != nil {
		panic(err)
	}

	for rowIndex, value := range lines {

		// trim any excess space of the lines.
		value := strings.TrimSpace(value)

		// split the string in to an array at every space
		arr := strings.Split(value, " ")

		// loop through the new array and convert it to an int
		// then create a gridPos data type and append it to the array
		for colIndex, val := range arr {
			x, _ := strconv.Atoi(val)
			gp := gridPos{
				Val: x,
				X:   colIndex,
				Y:   rowIndex,
			}
			numbers = append(numbers, gp)
		}
	}

	// create references for later!
	maxX = numbers[len(numbers)-1].X
	maxY = numbers[len(numbers)-1].Y

	// loop through each gridPosition to see the possibilities.
	for _, gp := range numbers {

		directions := []string{"down", "right", "diagonal-right", "diagonal-left"}

		for _, direction := range directions {
			if canGo(direction, gp) {
				nums := getAdjacent(direction, gp)

				// calculate the totals
				total := calculate(nums)

				// If the total is greater than the answer, make it the answer.
				if total > answer {
					answer = total
				}
			}
		}
	}
	fmt.Printf("Answer = %d\n", answer)
}

// Helper methods

// returns true if  there enough space to go down
func canGoDown(gp gridPos) bool {
	return gp.Y < (maxY - adjNumber)
}

// returns true if  there enough space to go right
func canGoRight(gp gridPos) bool {
	return gp.X < (maxX - adjNumber)
}

// returns true if  there enough space to go down and right
func canGoDiagonalRight(gp gridPos) bool {
	return canGoDown(gp) && canGoRight(gp)
}

// returns true if  there enough space to go down and left
func canGoDiagonalLeft(gp gridPos) bool {
	return canGoDown(gp) && gp.X >= adjNumber-1
}

// wrapper method to reduce code above.
func canGo(direction string, gp gridPos) bool {
	switch direction {
	case "down":
		return canGoDown(gp)
	case "right":
		return canGoRight(gp)
	case "diagonal-right":
		return canGoDiagonalRight(gp)
	case "diagonal-left":
		return canGoDiagonalLeft(gp)
	default:
		return canGoDown(gp)
	}
}

// Returns the numbers to calculate the product of depending on the direction specified.
func getAdjacent(direction string, gp gridPos) []gridPos {
	data := make([]gridPos, 0)
	selection := make([]coOrd, adjNumber)

	for i := 0; i < adjNumber; i++ {
		x := gp.X
		y := gp.Y

		if direction == "down" {
			y = gp.Y + i
		} else if direction == "right" {
			x = gp.X + i
		} else if direction == "diagonal-right" {
			y = gp.Y + i
			x = gp.X + i
		} else if direction == "diagonal-left" {
			x = gp.X - i
			y = gp.Y + i
		}
		selection[i] = coOrd{
			X: x,
			Y: y,
		}

	}

	for _, val := range numbers {

		l := gp.X - adjNumber
		r := gp.X + adjNumber
		t := gp.Y - adjNumber
		b := gp.Y + adjNumber

		// check if the val is within the adjNumber amount in each direction.
		if val.X < l || val.X > r || val.Y < t || val.Y > b {
			continue
		}

		if inArray(val, selection) {
			data = append(data, val)
			if len(data) == adjNumber {
				break
			}
		}
	}
	return data
}

func inArray(n gridPos, arr []coOrd) bool {
	for _, val := range arr {
		if n.X == val.X && n.Y == val.Y {
			return true
		}
	}
	return false
}

func calculate(gps []gridPos) int {
	total := 1
	for _, val := range gps {
		total *= val.Val
	}
	return total
}

// https://stackoverflow.com/a/18479916 - code snippet used from here.
// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
