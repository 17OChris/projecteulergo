package twentythree

import (
	"fmt"
)

const max = 28123

type Num struct {
	N          int
	isAbundant bool
}

func Run() {
	answer := 0
	allNumbers := make([]Num, max+1)
	abundantNumbers := make([]int, 0)

	allNumbers[0] = Num{N: 0, isAbundant: false}
	for i := 1; i <= max; i++ {
		if isAbundant(i) {
			allNumbers[i] = Num{
				N:          i,
				isAbundant: true,
			}
			abundantNumbers = append(abundantNumbers, i)
		} else {
			allNumbers[i] = Num{
				N:          i,
				isAbundant: false,
			}
		}
	}
	for i := 1; i <= max; i++ {
		if i < 24 {
			answer += i
			continue
		}
		canBeWritten := false
		for _, val := range abundantNumbers {
			if val > i {
				break
			}
			neededNumber := i - val
			if allNumbers[neededNumber].isAbundant {
				canBeWritten = true
				break
			}
		}

		if !canBeWritten {
			answer += i
		}
	}

	fmt.Printf("Answer = %d\n", answer)
}
func isAbundant(number int) bool {
	return addValues(getFactors(number)) > number
}

func addValues(numbers []int) int {
	total := 0
	for _, val := range numbers {
		total += val
	}
	return total
}
func getFactors(n int) []int {
	factors := make([]int, 0)
	if n == 1 {
		return factors
	}
	factors = append(factors, 1)

	for i := 2; i < n; i++ {
		if n%i == 0 {
			factors = append(factors, i)
		}
	}
	return factors
}
