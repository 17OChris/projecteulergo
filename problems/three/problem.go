package three

import (
	"fmt"
	"math"
)

const number = 600851475143

func Run() {
	halfNum := number
	if halfNum%2 != 0 {
		halfNum += 1
	}
	halfNum = (halfNum / 2) - 1
	answer := 0
	for i := 3; i < halfNum; i += 2 {

		if number%i != 0 {
			continue
		}
		k := number / i
		if isPrime(k) {
			answer = k
			break
		}
	}
	fmt.Printf("Answer = %d\n", answer)
}

// using https://www.planetmath.org/howtofindwhetheragivennumberisprimeornot
func isPrime(n int) bool {
	if n%2 == 0 {
		return false
	}
	k := int(math.Floor(math.Sqrt(float64(n))))
	for i := 3; i < k; i += 2 {
		if n%i == 0 {
			return false
		}
	}
	return true
}
