package eighteen

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/17OChris/projecteulergo/utils"
)

type Point struct {
	Val int
	Row int
	Pos int
}

var points []Point
var maxRow int = 0

func init() {
	populatePoints()
}

// this was my first attempt which was the brute force method.
func RunOld() {
	answer := recursive(points[0])
	fmt.Printf("Answer = %d\n", answer)
}

func recursive(p Point) int {
	total := p.Val

	if p.Row == maxRow {
		return total
	}

	a := recursive(getPoint(p.Row+1, p.Pos))
	b := recursive(getPoint(p.Row+1, p.Pos+1))

	if a > b {
		total += a
	} else {
		total += b
	}

	return total

}

func populatePoints() {
	points = make([]Point, 0)

	lines, err := utils.ReadLines("./problems/eighteen/numbers.txt")
	if err != nil {
		panic(err)
	}

	for rowIndex, line := range lines {
		arr := strings.Split(line, " ")

		for index, valString := range arr {
			val, _ := strconv.Atoi(valString)
			p := Point{
				Val: val,
				Row: rowIndex,
				Pos: index,
			}
			points = append(points, p)
		}
	}
	maxRow = points[len(points)-1].Row
}

func getPoint(row int, pos int) (p Point) {
	for _, val := range points {
		if val.Row == row && val.Pos == pos {
			p = val
			break
		}
	}
	return
}
