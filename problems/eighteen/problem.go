package eighteen

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/17OChris/projecteulergo/utils"
)

var rows [][]int

func init() {
	populateRows()
}

func Run() {
	answer := 0

	for rowIndex := len(rows) - 1; rowIndex >= 0; rowIndex-- {
		row := rows[rowIndex]

		for colIndex := 0; colIndex < len(row)-1; colIndex++ {

			a := row[colIndex]
			b := row[colIndex+1]

			if a > b {
				rows[rowIndex-1][colIndex] += a
			} else {
				rows[rowIndex-1][colIndex] += b
			}
		}
	}

	answer = rows[0][0]

	fmt.Printf("Answer = %d\n", answer)
}

func populateRows() {
	rows = make([][]int, 0)
	lines, err := utils.ReadLines("./problems/eighteen/numbers.txt")
	if err != nil {
		panic(err)
	}
	for _, line := range lines {
		arr := strings.Split(line, " ")

		row := make([]int, len(arr))

		for index, valString := range arr {
			val, _ := strconv.Atoi(valString)
			row[index] = val
		}
		rows = append(rows, row)
	}
}
