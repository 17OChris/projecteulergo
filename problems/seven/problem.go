package seven

import (
	"fmt"
	"math"
)

const aim = 10001

func Run() {
	answer := 0

	// starting at 1 so we can ignore "2"
	primeCount := 1

	// I will be the variable we increment.
	i := 3

	for primeCount != aim {
		if isPrime(i) {
			primeCount++
			answer = i
		}
		i += 2
	}

	fmt.Printf("Answer = %d\n", answer)
}

// using https://www.planetmath.org/howtofindwhetheragivennumberisprimeornot
func isPrime(n int) bool {
	if n%2 == 0 {
		return false
	}

	k := int(math.Floor(math.Sqrt(float64(n))))
	for i := 3; i <= k; i += 2 {
		if n%i == 0 {
			return false
		}
	}
	return true
}
