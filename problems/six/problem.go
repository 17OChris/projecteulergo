package six

import (
	"fmt"
)

// The target number
const max = 100

func Run() {
	// Find the difference between the sum of the squares
	// of the first one hundred natural
	// numbers and the square of the sum.
	answer := getSquareOfSum(max) - getSumOfSquaresRec(max)
	fmt.Printf("Answer = %d\n", answer)
}

// recursively decrease the number and add the square.
func getSumOfSquaresRec(n int) int {
	if n == 1 {
		return 1
	}
	return n*n + getSumOfSquaresRec(n-1)
}

// add all numbers up and then square them
func getSquareOfSum(n int) int {
	total := 0
	for i := n; i > 0; i-- {
		total += i
	}
	return total * total
}
