package two

import "fmt"

const max = 4000000

func Run() {
	first := 1
	second := 2
	sum := second
	next := 0

	for {
		next = first + second

		if next > max {
			break
		}

		if next%2 == 0 {
			sum += next
		}
		first = second
		second = next
	}
	fmt.Printf("Sum = %d\n", sum)
}
