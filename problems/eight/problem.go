package eight

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const length = 13

// we were told it was 1000 digits long from the question.
const amountOfNumbers = 1000

func Run() {
	answer := 0

	// Read the files to an a []string type
	lines, err := readLines("./problems/eight/text.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}

	// transform in to one large number array
	numbers := sortLinesToNumberArray(lines, amountOfNumbers)

	for k := 0; k+length <= len(numbers)-1; k++ {
		total := 1

		selection := numbers[k : k+length]

		// if theres a "0" in the selection, don't bother with the calculations
		if selectionIncludesZero(selection) {
			continue
		}

		for _, j := range selection {
			total *= j
		}

		if total > answer {
			answer = total
		}
	}

	fmt.Printf("Answer = %d\n", answer)
}

// https://stackoverflow.com/a/18479916 - code snippet used from here.
// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func sortLinesToNumberArray(lines []string, numberLength int) []int {
	numbers := make([]int, numberLength)
	// i will increase per number of each line.
	i := 0
	for _, val := range lines {
		l := strings.TrimSpace(val)
		for _, v := range l {
			// For speed, I am not checking for errors here.
			x, _ := strconv.Atoi(string(v))
			numbers[i] = x
			i++
		}
	}
	return numbers
}

func selectionIncludesZero(arr []int) bool {
	for _, val := range arr {
		if val == 0 {
			return true
		}
	}
	return false
}
