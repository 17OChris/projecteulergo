package utils

import (
	"math"
	"strconv"
)

// Here are where I have added useful methods I feel like I can reuse for other problems.

// I shall keep the code in the problem for first time soloutions, but I shall also add it here.

// Copied this method from an earlier problem
func AddTwoStrings(strOne string, strTwo string) string {
	// declare and initialise the total
	total := ""
	// Make sure both strings are the same size.

	if len(strOne) < len(strTwo) {
		for len(strOne) < len(strTwo) {
			strOne = "0" + strOne
		}
	} else if len(strTwo) < len(strOne) {
		for len(strTwo) < len(strOne) {
			strTwo = "0" + strTwo
		}
	}

	carryOver := 0

	// start from the back and convert each string to an int
	// then add together and work out the carry adjustment
	for i := len(strOne) - 1; i >= 0; i-- {
		a, _ := strconv.Atoi(string(strOne[i]))
		b, _ := strconv.Atoi(string(strTwo[i]))
		c := a + b + carryOver
		carryOver = 0

		for c >= 10 {
			c -= 10
			carryOver++
		}
		cS := strconv.Itoa(c)
		total = cS + total
	}

	// theres a chance that the last number has a carry,
	// So here we do one last check
	if carryOver > 0 {
		cS := strconv.Itoa(carryOver)
		total = cS + total
	}

	return total

}

// using https://www.planetmath.org/howtofindwhetheragivennumberisprimeornot
func IsPrime(n int) bool {
	if n%2 == 0 {
		return false
	}

	k := int(math.Floor(math.Sqrt(float64(n))))
	for i := 3; i <= k; i += 2 {
		if n%i == 0 {
			return false
		}
	}
	return true
}
