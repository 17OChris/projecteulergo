package main

import (
	"time"

	"gitlab.com/17OChris/projecteulergo/problems/twentysix"
	"gitlab.com/17OChris/projecteulergo/utils"
)

func main() {
	defer utils.TimeTrack(time.Now(), "Problem")
	twentysix.Run()
}
